# training data셋의 충분하지 않을 때
- data-agugmentation을 사용하여 data의 variation을 늘릴 수 있음

# ML trainings
    - supervised learning & unsupervised learning
        - dataset이 충분해야 함
    - reinforcement learning
        - dataset이 충분하지 않지만(필요 없음) 많은 simulation을 실행 할 수 있어야 함

# linear regression
- y = a * x + b
    - (a * x)의 a가 중요한 factor, b는 그냥 bias
- 함수의 dimention이 1차원

# generalized linear regression
- loss function을 정의하고 minimize하게 함
- 단순 linear regression는 표현력에 한계가 존재함
- linear, deming
- general한 값을 얻기 위해 regularization을 추가하여 robustness를 높이자
- general한 표현은 기존의 linear regression 공식이 padding data에 over-fitting 될 수 있으므로 임의로 추가한 variable이 반영됨
    - 

# logarithmic regression
- activate function(활성 함수) 0 ~ 1 True/False
- linear regression의 결과를 sigmoid 를 통해 일정 값 영역 안에 가둠

# SVM
- Support Vector Machine
- classifier 한 선분을 긋기 위한 2개의 벡터 (support vector)를 찾기 위한 과정
- 장점: 저차원, 고차원 데이터셋에 모두 잘 동작
- 단점: 데이터 샘플이 많으면 성능이 안 좋아짐, 데이터 전처리에 많은 비용이 필요함

# linear SVM
- 직선만으로 나누는 경우
- expand dimention을 통해 새로운 decision boundary을 새워 데이터 경향을 더 잘 표현할 수 있음
- linear 하다는 것은 (y =  x ^ n)의 표현을 말함

# nonlinear SVM
- log, y = x(x')
- 차원을 높인다(x)
- 선을 꼬아낸다

# kernal-trick
- polynomal
    - 1대1 방식으로 조합 가능한 모든 차수를 계산
    - Complex(nC2)
- gausian (RBF)
    - 1대다 방식으로 모든 차수를 다 계산함
    - gamma 값이 중요하다(너무 크면 over-fitting 될 수 있음)
    - Complex(n)
